class MainApp {
  constructor(title, content) {
    this.buttonShowContent = document.getElementById('show-content-btn')
    this.buttonHideContent = document.getElementById('hide-content-btn')
    this.divShowContent = document.getElementById('content')

    this.inputField = document.getElementById('inputField');
    this.divOutput = document.getElementById('output');

    this.buttonOpenModal = document.getElementById('open-modal-btn')
    this.buttonCloseModal = document.getElementById('close-modal-btn')
    this.myModal = document.getElementById('my-modal')

    this.buttonShowContent.addEventListener('click', this.handleShowContent.bind(this))
    this.buttonHideContent.addEventListener('click', this.handleHideContent.bind(this))
    this.buttonOpenModal.addEventListener('click', this.handleOpenModal.bind(this))
    this.buttonCloseModal.addEventListener('click', this.handleCloseModal.bind(this))

    this.inputField.addEventListener('input', this.handleInputChange.bind(this))

    this.setTitle(title)
    this.setContent(content)
    this.showTitle()
  }

  /**
   * Set title
   * @param title
   */
  setTitle(title) {
    if (!title) this.title = ''
    else this.title = title
  }

  /**
   * Get content
   * @param content
   */
  setContent(content) {
    if (!content) this.content = ''
    else this.content = content
  }

  /**
   * Get title
   * @returns {*}
   */
  getTitle() {
    return this.title
  }

  /**
   * Get content
   * @returns {*}
   */
  getContent() {
    return this.content
  }

  /**
   * get div content
   */
  getDivContent() {
    return this.divShowContent
  }

  /**
   * get button show content
   */
  getButtonShowContent() {
    return this.buttonShowContent
  }

  /**
   * get button hide content
   */
  getButtonHideContent() {
    return this.buttonHideContent
  }

  /**
   * get modal
   */
  getMyModal() {
    return this.myModal
  }

  /**
   * Show title
   */
  showTitle() {
    const title = this.getTitle()

    if (title) document.title = title
    else document.title = ''
  }

  /**
   * Handle show content
   */
  handleShowContent() {
    const content = this.getContent()
    if (!content) return

    this.divShowContent.style.display = 'block'
    this.buttonShowContent.classList.add('is-active')
    this.buttonHideContent.classList.remove('is-active')
    this.divShowContent.innerText = content
  }

  /**
   * Handle hide content
   */
  handleHideContent() {
    this.divShowContent.innerText = ''
    this.buttonShowContent.classList.remove('is-active')
    this.buttonHideContent.classList.add('is-active')
    this.divShowContent.style.display = 'none'
  }

  /**
   * Handle open modal
   */
  handleOpenModal(){
    this.myModal.style.display = 'block'
  }

  /**
   * Handle close modal
   */
  handleCloseModal(){
    this.myModal.style.display = 'none'
  }

  handleInputChange(e) {
    this.divOutput.textContent = e.target.value
  }
}

if (typeof window !== 'undefined') {
  window.onload = function () {
    mainApp = new MainApp(
      'CI/CD Automation Testing App',
      'Hello World',
      document
    );
  };
}

if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
  module.exports = MainApp;
}