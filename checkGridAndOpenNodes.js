const puppeteer = require('puppeteer');

async function checkGridAndOpenNodes() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const gridHubUrl = 'http://172.16.13.94:4444/grid/console';

  await page.goto(gridHubUrl);
  const gridStatus = await page.evaluate(() => {
    const pendingRequests = document.querySelector('.gridConsoleInfo p:first-of-type').textContent;
    return Number(pendingRequests.match(/\d+/)[0]);
  });

  if (gridStatus > 10) {
    console.log('Opening new nodes...');
  } else {
    console.log('No action needed. Number of pending requests is within threshold.');
  }

  await browser.close();
}

checkGridAndOpenNodes();