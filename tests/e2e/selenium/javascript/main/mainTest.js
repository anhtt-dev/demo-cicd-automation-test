const { Builder, By, Key, until } = require('selenium-webdriver');

async function example() {
  let driver = await new Builder().forBrowser('MicrosoftEdge').build();
  try {
    await driver.get('file:///E:/02.Training/CICD/demo-cicd-automation-test/index.html');

    let inputField = await driver.findElement(By.id('inputField'));
    await inputField.sendKeys('Hello, Selenium!', Key.RETURN);

    await driver.wait(until.elementTextIs(await driver.findElement(By.id('output')), 'Hello, Selenium!'), 50000);
    console.log('Test passed!');
  } catch (error) {
    console.error('Test failed:', error);
  } finally {
    await driver.quit();
  }
}

example();