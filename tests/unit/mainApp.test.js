const MainApp = require('../../js/MainApp.js');

describe('MainApp', () => {
  let mainApp;

  beforeEach(() => {
      document.body.innerHTML = `
          <div id="main">
              <button id="show-content-btn">Show Content</button>
              <button id="hide-content-btn">Hide Content</button>
              <button id="open-modal-btn">Open Modal</button>
    
              <div id="content"></div>
    
              <div id="my-modal" class="modal">
                  <div class="modal-content">
                      <span class="close">&times;</span>
                      <p>Some text in the Modal..</p>
                      <button id="close-modal-btn">Close</button>
                  </div>
              </div>
    
              <form>
                  <input id="inputField">
                  <div id="output"></div>
              </form>
          </div>
    `;
    mainApp = new MainApp('CI/CD Automation Testing App', 'Hello World');
  });

  describe('setTitle', () => {
    test('sets title correctly', () => {
      mainApp.setTitle('New Title1');
      expect(mainApp.getTitle()).toBe('New Title1');
    });

    test('does not set title if argument is falsy', () => {
      mainApp.setTitle('');
      expect(mainApp.getTitle()).toBe('');
    });
  });

  describe('setContent', () => {
    test('sets content correctly', () => {
      mainApp.setContent('New Content');
      expect(mainApp.getContent()).toBe('New Content');
    });

    test('does not set content if argument is falsy', () => {
      mainApp.setContent('');
      expect(mainApp.getContent()).toBe('');
    });
  });

  describe('showTitle', () => {
    test('sets document title if title is not empty', () => {
      mainApp.showTitle();
      expect(document.title).toBe('CI/CD Automation Testing App');
    });

    test('does not set document title if title is empty', () => {
      mainApp.setTitle('');
      mainApp.showTitle();
      expect(document.title).toBe('');
    });
  });

  describe('showContent', () => {
    test('display content when click button show content', () => {
      mainApp.handleShowContent();

      const divContent = mainApp.getDivContent();
      const buttonShowContent = mainApp.getButtonShowContent();
      const buttonHideContent = mainApp.getButtonHideContent();


      console.log(buttonShowContent,"buttonShowContent 1")
      console.log(buttonShowContent,"buttonHideContent 2")


      expect(divContent.innerText).toBe('Hello World');
      expect(divContent.style.display).toBe('block');
      expect(buttonShowContent.classList.contains('is-active')).toBe(true);
      expect(buttonHideContent.classList.contains('is-active')).toBe(false);
    });
  });

  describe('modal', () => {
    test('display modal when click button open', () => {
      mainApp.handleOpenModal();
      const modal = mainApp.getMyModal();

      expect(modal.style.display).toBe('block');
    });

    test('hidden modal when click button close', () => {
      mainApp.handleCloseModal();
      const modal = mainApp.getMyModal();

      expect(modal.style.display).toBe('none');
    });
  });
});